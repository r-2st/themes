# Themes

Various Alfred, Terminal, iTerm, and VSCode themes that I have created over the years.

## Alfred Themes

1. [Arkham Dark](https://github.com/phmullins/themes/tree/master/Themes/Alfred/Arkham/Arkham%20Dark)

## iTerm 2 Themes

1. [POP!_OS](https://github.com/phmullins/themes/tree/master/Themes/iTerm%202/Pop!_OS)

## Author
Created by [Patrick H. Mullins](http://www.pmullins.net). You can find me on  [Twitter](https://twitter.com/phmullins) and on [Telegram](https://telegram.org/) as @pmullins.

## License
Source is released under the MIT License (MIT) [license](license.md).
